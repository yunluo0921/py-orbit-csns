//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    CollimatorNew.hh
//
// AUTHOR
//    Xiaohan Lu
//
// CREATED
//    05/14/2020
//
// DESCRIPTION
//    A special class for Collimators. It defines a collimator which could 
//    set the position of four jaws (same shape) independently
//
///////////////////////////////////////////////////////////////////////////
#ifndef COLLIMATORNEW_H
#define COLLIMATORNEW_H

//pyORBIT utils
#include "CppPyWrapper.hh"
#include "Collimator.hh"
#include "Bunch.hh"

using namespace std;

/** 
  The collimator class is used to define how a bunch propogates through a collimator
*/
    
class CollimatorNew: public Collimator
{
public:
	
	/** Constructor */
	CollimatorNew(double length, int ma, 
		              double density_fac, int shape, 
									double a, double b, double c, double d, double e, double f, double angle, double pos);

private:

	/** check to see if the particle is inside the collimator */
	int checkCollFlag(double x, double y);

	/** check the particle stepsize. Reset it if necessary  */
	void checkStep(double rl, double radlengthfac, double& stepsize, double* coords, SyncPart* syncpart);
	
	/** get the particle beta */
	double getBeta(double* coords, SyncPart* syncpart);
	
	/** get the particle momentum */
	double getP(double* coords, SyncPart* syncpart);

protected:
	//Collimator parameters
	double e_, f_;
};

//end of COLLIMATORNEW_H ifdef
#endif

