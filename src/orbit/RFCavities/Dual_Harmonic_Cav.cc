#include "Dual_Harmonic_Cav.hh"
#include "ParticleMacroSize.hh"

#include <iostream>
#include <cmath>

#include "Bunch.hh"
#include "OrbitConst.hh"

using namespace OrbitUtils;

// Constructor
Dual_Harmonic_Cav::Dual_Harmonic_Cav(double ZtoPhi   ,
                           double dESync   ,
                           double RFHNum   ,
                           double RatioRFHNum,
                           double RFVoltage,
                           double RatioVoltage,
                           double RFPhase,
                           double RFPhase1,
                           double RFPhase2,
                           double GammaTrans,
                           int longTrackOnly): CppPyWrapper(NULL)
{
  _ZtoPhi    = ZtoPhi;
  _dESync    = dESync;
  _RFHNum    = RFHNum;
  _RatioRFHNum = RatioRFHNum;
  _RFVoltage = RFVoltage;
  _RatioVoltage = RatioVoltage;
  _RFPhase   = RFPhase;
  _RFPhase1   = RFPhase1;
  _RFPhase2   = RFPhase2;
  _GammaTrans = GammaTrans;
  _longTrackOnly = longTrackOnly;

}

// Destructor
Dual_Harmonic_Cav::~Dual_Harmonic_Cav()
{
}

void Dual_Harmonic_Cav::setZtoPhi(double ZtoPhi)
{
  _ZtoPhi = ZtoPhi;
}

double Dual_Harmonic_Cav::getZtoPhi()
{
  return _ZtoPhi;
}

void Dual_Harmonic_Cav::setdESync(double dESync)
{
  _dESync = dESync;
}

double Dual_Harmonic_Cav::getdESync()
{
  return _dESync;
}
void Dual_Harmonic_Cav::setRatioRFHNum(double RatioRFHNum)
{
  _RatioRFHNum = RatioRFHNum;
}

double Dual_Harmonic_Cav::getRatioRFHNum()
{
  return _RatioRFHNum;
}

void Dual_Harmonic_Cav::setRFHNum(double RFHNum)
{
 _RFHNum = RFHNum;
}

double Dual_Harmonic_Cav::getRFHNum()
{
  return _RFHNum;
}

void Dual_Harmonic_Cav::setRFVoltage(double RFVoltage)
{
  _RFVoltage = RFVoltage;
}

double Dual_Harmonic_Cav::getRFVoltage()
{
  return _RFVoltage;
}

void Dual_Harmonic_Cav::setRatioVoltage(double RatioVoltage)
{
  _RatioVoltage = RatioVoltage;
}

double Dual_Harmonic_Cav::getRatioVoltage()
{
  return _RatioVoltage;
}

void Dual_Harmonic_Cav::setRFPhase(double RFPhase)
{
  _RFPhase = RFPhase;

}

double Dual_Harmonic_Cav::getRFPhase()
{
  return _RFPhase;
}
void Dual_Harmonic_Cav::setRFPhase1(double RFPhase1)
{
  _RFPhase1 = RFPhase1;

}
void Dual_Harmonic_Cav::setRFPhase2(double RFPhase2)
{
  _RFPhase2 = RFPhase2;

}

double Dual_Harmonic_Cav::getRFPhase1()
{
  return _RFPhase1;
}
double Dual_Harmonic_Cav::getRFPhase2()
{
  return _RFPhase2;
}
void Dual_Harmonic_Cav::setOldGamma(double Gamma)
{
    _OldGamma = Gamma;
}

double Dual_Harmonic_Cav::getOldGamma()
{
    return _OldGamma;
}

void Dual_Harmonic_Cav::setOldBeta(double Beta)
{
    _OldBeta = Beta;
}

double Dual_Harmonic_Cav::getOldBeta()
{
    return _OldBeta;
}
void Dual_Harmonic_Cav::setGammaTrans(double GammaTrans)
{
    _GammaTrans = GammaTrans;
}

double Dual_Harmonic_Cav::getGammaTrans()
{
    return _GammaTrans;
}

void Dual_Harmonic_Cav::trackBunch(Bunch* bunch)
{
  double ZtoPhi    = _ZtoPhi;
  double dESync    = _dESync;
  double OldGamma  = _OldGamma;
  double OldBeta   = _OldBeta;
  double RFHNum    = _RFHNum;
  double RatioRFHNum = _RatioRFHNum;
  double RFVoltage = _RFVoltage;
  double RatioVoltage = _RatioVoltage;
  double RFPhase   = OrbitConst::PI * _RFPhase / 180.0;
  double RFPhase1 = OrbitConst::PI * _RFPhase1 / 180.0;
  double RFPhase2 = OrbitConst::PI * _RFPhase2 / 180.0;
  double GammaTrans = _GammaTrans;
  int longTrackOnly = _longTrackOnly;


  double dESum, phase,etaCompaction,dp_p;

  bunch->compress();
  SyncPart* syncPart = bunch->getSyncPart();
  double** arr = bunch->coordArr();
  double Gamma = syncPart->getGamma();
  double Beta = syncPart->getBeta();
  double dppFac = 1. /((Beta*Beta)*bunch->getMass()*Gamma);
  if(longTrackOnly == 1)
    {
      if(GammaTrans < 0.0)
      {
        etaCompaction = -(1. / GammaTrans/GammaTrans) -
                              (1. / Gamma/ GammaTrans);
      }
      else
      {
        etaCompaction = (1. / GammaTrans/ GammaTrans) -
                              (1. / Gamma/Gamma);
      }
      for(int i = 0; i < bunch->getSize(); i++)
      {
        phase = -ZtoPhi * arr[i][4] * RFHNum ;
        dESum  = bunch->getCharge()* RFVoltage* (sin(RFPhase1 + phase)  - RatioVoltage *  sin(RFPhase2 + RatioRFHNum * (phase - RFPhase)) ); 
        arr[i][5] = Beta/OldBeta*(arr[i][5] + dESum - dESync);
        dp_p = arr[i][5] * dppFac;
        phase += 2*OrbitConst::PI * RFHNum* etaCompaction*dp_p;
        if(phase >  OrbitConst::PI) phase -= 2*OrbitConst::PI;
        if(phase < -OrbitConst::PI) phase += 2*OrbitConst::PI;
        arr[i][4] = -phase/ZtoPhi/RFHNum;
      }
    }
  else
  {
      for(int i = 0; i < bunch->getSize(); i++)
      {
        phase = -ZtoPhi * arr[i][4] * RFHNum ;
        dESum  = bunch->getCharge()* RFVoltage * (sin(RFPhase1 + phase)  - RatioVoltage *  sin(RFPhase2 + RatioRFHNum * (phase - RFPhase)) ); 
        arr[i][5] = Beta/OldBeta*(arr[i][5] + dESum - dESync);
        arr[i][1] *= OldGamma*OldBeta/(Gamma*Beta);
        arr[i][3] *= OldGamma*OldBeta/(Gamma*Beta);
      }
  }
}
