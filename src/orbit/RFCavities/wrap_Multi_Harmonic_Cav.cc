#include "orbit_mpi.hh"
#include "pyORBIT_Object.hh"

#include "wrap_Multi_Harmonic_Cav.hh"
#include "wrap_bunch.hh"

#include <iostream>

#include "Multi_Harmonic_Cav.hh"

using namespace OrbitUtils;

namespace wrap_rfcavities
{

#ifdef __cplusplus
extern "C"
{
#endif

//---------------------------------------------------------
// Python Multi_Harmonic_Cav class definition
//---------------------------------------------------------

//-----------------------------------------------------
// Constructor for python class wrapping Multi_Harmonic_Cav instance
// It never will be called directly
//-----------------------------------------------------

static PyObject* Multi_Harmonic_Cav_new(PyTypeObject *type,
                                  PyObject *args,
                                  PyObject *kwds)
{
  pyORBIT_Object* self;
  self = (pyORBIT_Object*) type->tp_alloc(type, 0);
  self->cpp_obj = NULL;
  return (PyObject*) self;
}

//-----------------------------------------------------
// Initialization for python Multi_Harmonic_Cav class
// This is implementation of the __init__ method
//-----------------------------------------------------

static int Multi_Harmonic_Cav_init(pyORBIT_Object *self,
                             PyObject *args,
                             PyObject *kwds)
{
  double ZtoPhi       = 0.0;
  double dESync        = 0.0;
  int nRFHarmonics    = 0;
  PyObject* objh;
  PyObject* objv;
  PyObject* objp;

  if(!PyArg_ParseTuple(args, "ddOOOi:arguments",
                       &ZtoPhi,
                       &dESync,
                       &objh,
                       &objv,
                       &objp,
                       &nRFHarmonics))
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav - Multi_Harmonic_Cav_init - cannot parse arguments! They should be (ZtoPhi, dESync, RFHarms, RFVoltages, RFPhases, nRFHarmonics)");
  }
  double RFHarms[nRFHarmonics];
  double RFVoltages[nRFHarmonics];
  double RFPhases[nRFHarmonics];
  PyObject *iterh = PyObject_GetIter(objh);
  PyObject *iterv = PyObject_GetIter(objv);
  PyObject *iterp = PyObject_GetIter(objp);
  if(!iterh or !iterv or !iterp)
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_init - RFHarms, RFVoltages and RFPhases must be iterable!");
  }
  int len=0;
  while(1)
  {
      PyObject *nexth = PyIter_Next(iterh);
      PyObject *nextv = PyIter_Next(iterv);
      PyObject *nextp = PyIter_Next(iterp);
      if(!nexth or !nextv or !nextp){
          break;
      }
      if(PyFloat_Check(nexth) and PyFloat_Check(nextv) and PyFloat_Check(nextp)){
          double elemh = PyFloat_AsDouble(nexth);
          double elemv = PyFloat_AsDouble(nextv);
          double elemp = PyFloat_AsDouble(nextp);
          RFHarms[len] = elemh;
          RFVoltages[len] = elemv;
          RFPhases[len] = elemp;
          len++;
      }
  }
  if(nRFHarmonics != len)
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_init - the size of RFHarms, RFVoltages and RFPhases should be equal to nRFHarmonics!");
  }

  self->cpp_obj = new Multi_Harmonic_Cav(ZtoPhi,
                                   dESync,
                                   RFHarms,
                                   RFVoltages,
                                   RFPhases,
                                   nRFHarmonics);
  ((Multi_Harmonic_Cav*) self->cpp_obj)->setPyWrapper((PyObject*) self);
  return 0;
}

//-----------------------------------------------------
// Destructor for python Multi_Harmonic_Cav class (__del__ method)
//-----------------------------------------------------

static void Multi_Harmonic_Cav_del(pyORBIT_Object* self)
{
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) self->cpp_obj;
  delete cpp_Multi_Harmonic_Cav;
  self->ob_type->tp_free((PyObject*) self);
}

//-----------------------------------------------------
// Accessor routines:
// Sets or returns value depending on the number of arguments
// if nVars == 1 set value
// if nVars == 0 get value:
// variable(value) - sets new value
// variable()      - returns value
// These are implementations of the set(value) and get() methods
//-----------------------------------------------------

static PyObject* Multi_Harmonic_Cav_ZtoPhi(PyObject *self, PyObject *args)
{
  pyORBIT_Object* pyMulti_Harmonic_Cav = (pyORBIT_Object*) self;
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) pyMulti_Harmonic_Cav->cpp_obj;
  int nVars = PyTuple_Size(args);
  double val = 0.;
  if(nVars == 1)
  {
    if(!PyArg_ParseTuple(args,"d:ZtoPhi", &val))
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_ZtoPhi(value) - value is needed");
    }
    cpp_Multi_Harmonic_Cav->setZtoPhi(val);
    return Py_BuildValue("d", val);
  }
  else if(nVars == 0)
  {
    val = cpp_Multi_Harmonic_Cav->getZtoPhi();
    return Py_BuildValue("d", val);
  }
  else
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_ZtoPhi. You should call ZtoPhi() or ZtoPhi(value)");
  }
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject* Multi_Harmonic_Cav_dESync(PyObject *self, PyObject *args)
{
  pyORBIT_Object* pyMulti_Harmonic_Cav = (pyORBIT_Object*) self;
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) pyMulti_Harmonic_Cav->cpp_obj;
  int nVars = PyTuple_Size(args);
  double val = 0.;
  if(nVars == 1)
  {
    if(!PyArg_ParseTuple(args,"d:dESync", &val))
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_dESync(value) - value is needed");
    }
    cpp_Multi_Harmonic_Cav->setdESync(val);
    return Py_BuildValue("d", val);
  }
  else if(nVars == 0)
  {
    val = cpp_Multi_Harmonic_Cav->getdESync();
    return Py_BuildValue("d", val);
  }
  else
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_dESync. You should call dESync() or dESync(value)");
  }
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject* Multi_Harmonic_Cav_RFVoltages(PyObject *self, PyObject *args)
{
  pyORBIT_Object* pyMulti_Harmonic_Cav = (pyORBIT_Object*) self;
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) pyMulti_Harmonic_Cav->cpp_obj;
  int nVars = PyTuple_Size(args);
  int size = cpp_Multi_Harmonic_Cav->getnRFHarmonics();
  PyObject* tuple = PyTuple_New(size);
  double valv[size];
  if(nVars == 1)
  {
    PyObject *obj;
    if(!PyArg_ParseTuple(args,"O:RFVoltages", &obj))
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFVoltages(value) - value is needed");
    }
    PyObject *iter = PyObject_GetIter(obj);
    if(!iter)
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFVoltages(value) - value must be iterable!");
    }
    int len=0;
    while(1)
    {
        PyObject *next = PyIter_Next(iter);
        if(!next){
            break;
        }
        if(PyFloat_Check(next)){
            double elem = PyFloat_AsDouble(next);
            valv[len] = elem;
            len++;
        }
    }
    if(size != len)
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFVoltages(value) - the size of value should be equal to nRFHarmonics!");
    }
    for (int i=0; i< len; i++)
    {
        double elem = valv[i];
        PyTuple_SetItem(tuple, i, Py_BuildValue("d", elem));
    }
    cpp_Multi_Harmonic_Cav->setRFVoltages(valv);
    return tuple;
  }
  else if(nVars == 0)
  {
    double* rfv = cpp_Multi_Harmonic_Cav->getRFVoltages();
    for (int i=0; i< size; i++)
    {
        double elem = rfv[i];
        PyTuple_SetItem(tuple, i, Py_BuildValue("d", elem));
    }
    return tuple;
  }
  else
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFVoltages. You should call RFVoltages() or RFVoltages(value)");
  }
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject* Multi_Harmonic_Cav_RFPhases(PyObject *self, PyObject *args)
{
  pyORBIT_Object* pyMulti_Harmonic_Cav = (pyORBIT_Object*) self;
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) pyMulti_Harmonic_Cav->cpp_obj;
  int nVars = PyTuple_Size(args);
  int size = cpp_Multi_Harmonic_Cav->getnRFHarmonics();
  PyObject* tuple = PyTuple_New(size);
  double valp[size];
  if(nVars == 1)
  {
    PyObject *obj;
    if(!PyArg_ParseTuple(args,"O:RFPhases", &obj))
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFPhases(value) - value(list) is needed");
    }
    PyObject *iter = PyObject_GetIter(obj);
    if(!iter)
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFPhases(value) - value must be iterable!");
    }
    int len=0;
    while(1)
    {
        PyObject *next = PyIter_Next(iter);
        if(!next){
            break;
        }
        if(PyFloat_Check(next)){
            double elem = PyFloat_AsDouble(next);
            valp[len] = elem;
            len++;
        }
    }
    if(size != len)
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFPhases(value) - the size of value should be equal to nRFHarmonics!");
    }
    for (int i=0; i< len; i++)
    {
        double elem = valp[i];
        PyTuple_SetItem(tuple, i, Py_BuildValue("d", elem));
    }
    cpp_Multi_Harmonic_Cav->setRFPhases(valp);
    return tuple;
  }
  else if(nVars == 0)
  {
    double* rfp = cpp_Multi_Harmonic_Cav->getRFPhases();
    for (int i=0; i< size; i++)
    {
        double elem = rfp[i];
        PyTuple_SetItem(tuple, i, Py_BuildValue("d", elem));
    }
    return tuple;
  }
  else
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_RFPhases. You should call RFPhases() or RFPhases(value)");
  }
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject* Multi_Harmonic_Cav_OldGamma(PyObject *self, PyObject *args)
{
  pyORBIT_Object* pyMulti_Harmonic_Cav = (pyORBIT_Object*) self;
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) pyMulti_Harmonic_Cav->cpp_obj;
  int nVars = PyTuple_Size(args);
  double val = 0.;
  if(nVars == 1)
  {
    if(!PyArg_ParseTuple(args,"d:OldGamma", &val))
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_OldGamma(value) - value is needed");
    }
    cpp_Multi_Harmonic_Cav->setOldGamma(val);
    return Py_BuildValue("d", val);
  }
  else if(nVars == 0)
  {
    val = cpp_Multi_Harmonic_Cav->getOldGamma();
    return Py_BuildValue("d", val);
  }
  else
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_OldGamma. You should call OldGamma() or OldGamma(value)");
  }
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject* Multi_Harmonic_Cav_OldBeta(PyObject *self, PyObject *args)
{
  pyORBIT_Object* pyMulti_Harmonic_Cav = (pyORBIT_Object*) self;
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) pyMulti_Harmonic_Cav->cpp_obj;
  int nVars = PyTuple_Size(args);
  double val = 0.;
  if(nVars == 1)
  {
    if(!PyArg_ParseTuple(args,"d:OldBeta", &val))
    {
      ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_OldBeta(value) - value is needed");
    }
    cpp_Multi_Harmonic_Cav->setOldBeta(val);
    return Py_BuildValue("d", val);
  }
  else if(nVars == 0)
  {
    val = cpp_Multi_Harmonic_Cav->getOldBeta();
    return Py_BuildValue("d", val);
  }
  else
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav_OldBeta. You should call OldBeta() or OldBeta(value)");
  }
  Py_INCREF(Py_None);
  return Py_None;
}

//-----------------------------------------------------
// trackBunch(Bunch* bunch)
//-----------------------------------------------------

static PyObject* Multi_Harmonic_Cav_trackBunch(PyObject *self, PyObject *args)
{
  pyORBIT_Object* pyMulti_Harmonic_Cav = (pyORBIT_Object*) self;
  Multi_Harmonic_Cav* cpp_Multi_Harmonic_Cav = (Multi_Harmonic_Cav*) pyMulti_Harmonic_Cav->cpp_obj;
  PyObject* pyBunch;
  if(!PyArg_ParseTuple(args, "O:trackBunch", &pyBunch))
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav - trackBunch(Bunch* bunch) - parameter is needed.");
  }
  PyObject* pyORBIT_Bunch_Type = wrap_orbit_bunch::getBunchType("Bunch");
  if(!PyObject_IsInstance(pyBunch, pyORBIT_Bunch_Type))
  {
    ORBIT_MPI_Finalize("PyMulti_Harmonic_Cav - trackBunch(Bunch* bunch) - the parameter should be a Bunch.");
  }
  Bunch* cpp_bunch = (Bunch*) ((pyORBIT_Object*) pyBunch)->cpp_obj;
  cpp_Multi_Harmonic_Cav->trackBunch(cpp_bunch);
  Py_INCREF(Py_None);
  return Py_None;
}

//-----------------------------------------------------
// Definition of the methods of the python Multi_Harmonic_Cav wrapper class
// They will be available at python level
//-----------------------------------------------------

static PyMethodDef Multi_Harmonic_CavClassMethods[] =
{
  { "ZtoPhi", Multi_Harmonic_Cav_ZtoPhi ,METH_VARARGS,"Set ZtoPhi(value) or get ZtoPhi() the conversion from bunch longitudinal length coordinate to cavity phase"},
  { "dESync", Multi_Harmonic_Cav_dESync ,METH_VARARGS,"Set dESync(value) or get dESync() the energy change of the synchronous particle"},
  { "RFVoltages", Multi_Harmonic_Cav_RFVoltages ,METH_VARARGS,"Set RFVoltages(value) or get RFVoltages() all RF cavity voltages (each order) in GeV"},
  { "RFPhases", Multi_Harmonic_Cav_RFPhases ,METH_VARARGS,"Set RFPhases(value) or get RFPhases() all RF cavity phase in radians"},
  { "OldGamma", Multi_Harmonic_Cav_OldGamma ,METH_VARARGS,"Set OldGamma(value) or get OldGamma() the old gamma value of syncpart"},
  { "OldBeta", Multi_Harmonic_Cav_OldBeta ,METH_VARARGS,"Set OldBeta(value) or get OldBeta() the old beta value of syncpart"},
  {"trackBunch", Multi_Harmonic_Cav_trackBunch, METH_VARARGS, "tracks the Bunch through a harmonic RF cavity"},
  {NULL}
};

//-----------------------------------------------------
// Definition of the members of the python Multi_Harmonic_Cav wrapper class
// They will be available at python level
//-----------------------------------------------------

static PyMemberDef Multi_Harmonic_CavClassMembers [] =
{
  {NULL}
};

//-----------------------------------------------------
//new python Multi_Harmonic_Cav wrapper type definition
//-----------------------------------------------------

static PyTypeObject pyORBIT_Multi_Harmonic_Cav_Type =
{
  PyObject_HEAD_INIT(NULL)
  0, /*ob_size*/
  "Multi_Harmonic_Cav", /*tp_name*/
  sizeof(pyORBIT_Object), /*tp_basicsize*/
  0, /*tp_itemsize*/
  (destructor) Multi_Harmonic_Cav_del , /*tp_dealloc*/
  0, /*tp_print*/
  0, /*tp_getattr*/
  0, /*tp_setattr*/
  0, /*tp_compare*/
  0, /*tp_repr*/
  0, /*tp_as_number*/
  0, /*tp_as_sequence*/
  0, /*tp_as_mapping*/
  0, /*tp_hash */
  0, /*tp_call*/
  0, /*tp_str*/
  0, /*tp_getattro*/
  0, /*tp_setattro*/
  0, /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
  "The Multi_Harmonic_Cav python wrapper", /* tp_doc */
  0, /* tp_traverse */
  0, /* tp_clear */
  0, /* tp_richcompare */
  0, /* tp_weaklistoffset */
  0, /* tp_iter */
  0, /* tp_iternext */
  Multi_Harmonic_CavClassMethods, /* tp_methods */
  Multi_Harmonic_CavClassMembers, /* tp_members */
  0, /* tp_getset */
  0, /* tp_base */
  0, /* tp_dict */
  0, /* tp_descr_get */
  0, /* tp_descr_set */
  0, /* tp_dictoffset */
  (initproc) Multi_Harmonic_Cav_init, /* tp_init */
  0, /* tp_alloc */
  Multi_Harmonic_Cav_new, /* tp_new */
};

//--------------------------------------------------
//Initialization function of the pyMulti_Harmonic_Cav class
//It will be called from Bunch wrapper initialization
//--------------------------------------------------

void initMulti_Harmonic_Cav(PyObject* module)
{
  if (PyType_Ready(&pyORBIT_Multi_Harmonic_Cav_Type) < 0) return;
  Py_INCREF(&pyORBIT_Multi_Harmonic_Cav_Type);
  PyModule_AddObject(module, "Multi_Harmonic_Cav", (PyObject*) &pyORBIT_Multi_Harmonic_Cav_Type);
}

#ifdef __cplusplus
}
#endif

//end of namespace wrap_rfcavities
}
