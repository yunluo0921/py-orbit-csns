#include "Harmonic_Cav.hh"
#include "ParticleMacroSize.hh"

#include <iostream>
#include <cmath>

#include "Bunch.hh"
#include "OrbitConst.hh"

using namespace OrbitUtils;

// Constructor
Harmonic_Cav::Harmonic_Cav(double ZtoPhi   ,
                           double dESync   ,
                           double RFHNum   ,
                           double RFVoltage,
                           double RFPhase): CppPyWrapper(NULL)
{
  _ZtoPhi    = ZtoPhi;
  _dESync    = dESync;
  _RFHNum    = RFHNum;
  _RFVoltage = RFVoltage;
  _RFPhase   = RFPhase;
}

// Destructor
Harmonic_Cav::~Harmonic_Cav()
{
}

void Harmonic_Cav::setZtoPhi(double ZtoPhi)
{
  _ZtoPhi = ZtoPhi;
}

double Harmonic_Cav::getZtoPhi()
{
  return _ZtoPhi;
}

void Harmonic_Cav::setdESync(double dESync)
{
  _dESync = dESync;
}

double Harmonic_Cav::getdESync()
{
  return _dESync;
}

void Harmonic_Cav::setRFHNum(double RFHNum)
{
  _RFHNum = RFHNum;
}

double Harmonic_Cav::getRFHNum()
{
  return _RFHNum;
}

void Harmonic_Cav::setRFVoltage(double RFVoltage)
{
  _RFVoltage = RFVoltage;
}

double Harmonic_Cav::getRFVoltage()
{
  return _RFVoltage;
}

void Harmonic_Cav::setRFPhase(double RFPhase)
{
  _RFPhase = RFPhase;
}

double Harmonic_Cav::getRFPhase()
{
  return _RFPhase;
}

void Harmonic_Cav::setOldGamma(double Gamma)
{
    _OldGamma = Gamma;
}

double Harmonic_Cav::getOldGamma()
{
    return _OldGamma;
}

void Harmonic_Cav::setOldBeta(double Beta)
{
    _OldBeta = Beta;
}

double Harmonic_Cav::getOldBeta()
{
    return _OldBeta;
}

void Harmonic_Cav::trackBunch(Bunch* bunch)
{
  double ZtoPhi    = _ZtoPhi;
  double dESync    = _dESync;
  double RFHNum    = _RFHNum;
  double RFVoltage = _RFVoltage;
  double OldGamma  = _OldGamma;
  double OldBeta   = _OldBeta;
  double RFPhase   = OrbitConst::PI * _RFPhase / 180.0;

  double dERF, phase;

  bunch->compress();
  SyncPart* syncPart = bunch->getSyncPart();
  double** arr = bunch->coordArr();
  double Gamma = syncPart->getGamma();
  double Beta = syncPart->getBeta();

  for(int i = 0; i < bunch->getSize(); i++)
  {
    phase = -ZtoPhi * arr[i][4];
    dERF  = bunch->getCharge()* RFVoltage * sin(RFHNum * phase + RFPhase);
    arr[i][5] += dERF - dESync;
    arr[i][1] *= OldGamma*OldBeta/(Gamma*Beta);
    arr[i][3] *= OldGamma*OldBeta/(Gamma*Beta);
  }
}
