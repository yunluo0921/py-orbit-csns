"""
Module. Includes classes for RF accelerator nodes.
"""

import sys
import os
import math

# import the function that finalizes the execution
from orbit.utils import orbitFinalize

# import general accelerator elements and lattice
from orbit.lattice import AccLattice, AccNode,\
AccActionsContainer, AccNodeBunchTracker

# import teapot drift class
from orbit.teapot import DriftTEAPOT

#import RF cavity classes
from rfcavities import Frequency_Cav
from rfcavities import Harmonic_Cav
from rfcavities import Multi_Harmonic_Cav
from rfcavities import Barrier_Cav
from rfcavities import Dual_Harmonic_Cav

class Base_RFNode(DriftTEAPOT):

	def __init__(self, length, name = "base_rfnode"):
		"""
			Constructor. Creates Base RF Cavity TEAPOT element.
			It will never be called.
		"""
		DriftTEAPOT.__init__(self, name)
		self.setType("base rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#put the track method here:
		#self..trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#put the track method here:
		#self..trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

        def rtbi(self, spr, func, x1, x2, tol):
                '''find the root of a function'''
                tiny = 1e-20
                xl = x1
                xh = x2
                fh = func(x2)
                fl = func(x1)
                sp = spr[0]
                if fh*fl > 0:
                    return 0.0
                if abs(fh) < tiny:
                    sp = x2
                    return 1
                if abs(fl) < tiny:
                    sp = x1
                    return 1
                for i in xrange(300):
                    dx = xh-xl
                    xmid = 0.5*(xh+xl)
                    fmid = func(xmid)
                    if abs(dx) < tol or abs(fmid) < tiny:
                        spr[0] = xmid
                        return 1
                    if fmid*fh < 0:
                        xl = xmid
                        fl = fmid
                    elif fmid*fl < 0:
                        xh = xmid
                        fh = fmid
                return 0


class Frequency_RFNode(Base_RFNode):

	def __init__(self, RFFreq, RFE0TL, RFPhase,\
		length, name = "frequency_rfnode"):
		"""
			Constructor. Creates Frequency
			RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.frequencynode = Frequency_Cav(RFFreq, RFE0TL, RFPhase)
		self.setType("frequency rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#put the track method here:
		self.frequencynode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#put the track method here:
		self.frequencynode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, dESync, RFHNum, RFVoltage, RFPhase,\
		bunch, length, name = "harmonic_rfnode"):
		"""
			Constructor. Creates Harmonic
			RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		self.harmonicnode = Harmonic_Cav(ZtoPhi, dESync,\
			RFHNum, RFVoltage, RFPhase)
                self.harmonicnode.OldGamma(gamma)
                self.harmonicnode.OldBeta(beta)
		self.setType("harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class Dual_Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, dESync, RFHNum, RatioRFHNum, RFVoltage, RatioVoltage, SyncPhase, RFPhase1, RFPhase2, gammaTrans, longTrackOnly, length, name = "harmonic_rfnode"):
		"""
			Constructor. Creates Harmonic
			RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		longOnly = int(longTrackOnly)
		self.dualharmonicnode = Dual_Harmonic_Cav(ZtoPhi, dESync, RFHNum, RatioRFHNum, RFVoltage, RatioVoltage, SyncPhase, RFPhase1, RFPhase2, gammaTrans, longTrackOnly)
		self.setType("dual harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#put the track method here:
		self.dualharmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#put the track method here:
		self.dualharmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class Multi_Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, RFHNum, RatioRFHNum, RFVoltage, RatioVoltage, RFPhase, RFPhase2,\
		length, name = "harmonic_rfnode"):
		"""
			Constructor. Creates Harmonic
			RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.multiharmonicnode = Multi_Harmonic_Cav(ZtoPhi, \
			RFHNum, RatioRFHNum, RFVoltage, RatioVoltage, RFPhase, RFPhase2)
		self.setType("dual harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#put the track method here:
		self.multiharmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#put the track method here:
		self.multiharmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class BRhoDep_Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, accelDict, bunch,\
		length, name = "brho_timedep_harmonic_rfnode"):
		"""
			Constructor. Creates BRho Time Dependent
			Harmonic RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.Z2Phi = ZtoPhi
		self.localDict = accelDict
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		BRho_tuple = self.localDict["BRho"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		keold = bunch.getSyncParticle().kinEnergy()
		eold = keold + mass
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		BRho = interp(time, n_tuple,\
			time_tuple, BRho_tuple)
		pcnew = 0.299792458 * charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		dESync = enew - eold
		Zsync = syncZ(ZtoPhi, gammaTrans, gamma, charge,\
			dESync, RFHNum, RFVoltage, RFPhase)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode = Harmonic_Cav(ZtoPhi, dESync,\
			RFHNum, RFVoltage, RFPhase)
		self.setType("harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		BRho_tuple = self.localDict["BRho"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		eold = keold + mass
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		BRho = interp(time, n_tuple,\
			time_tuple, BRho_tuple)
		pcnew = 0.299792458 * charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		dESync = enew - eold
		ZtoPhi = self.Z2Phi
		Zsync = syncZ(ZtoPhi, gammaTrans, gamma, charge,\
			dESync, RFHNum, RFVoltage, RFPhase)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.RFVoltage(RFVoltage)
		self.harmonicnode.RFPhase(RFPhase)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		BRho_tuple = self.localDict["BRho"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		eold = keold + mass
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		BRho = interp(time, n_tuple,\
			time_tuple, BRho_tuple)
		pcnew = 0.299792458 * charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		dESync = enew - eold
		ZtoPhi = self.Z2Phi
		Zsync = syncZ(ZtoPhi, gammaTrans, gamma, charge,\
			dESync, RFHNum, RFVoltage, RFPhase)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.RFVoltage(RFVoltage)
		self.harmonicnode.RFPhase(RFPhase)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class SyncPhaseDep_Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, accelDict, bunch,\
		length, name = "syncphase_timedep_harmonic_rfnode"):
		"""
			Constructor. Creates SyncPhase Time Dependent
			Harmonic RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.Z2Phi = ZtoPhi
		self.localDict = accelDict
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		SyncPhase_tuple = self.localDict["SyncPhase"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		keold = bunch.getSyncParticle().kinEnergy()
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		SyncPhase = interp(time, n_tuple,\
			time_tuple, SyncPhase_tuple)
		dESync = charge * RFVoltage *\
			math.sin(math.pi *\
			(RFHNum * SyncPhase + RFPhase) / 180.0)
		kenew = keold + dESync
		bunch.getSyncParticle().kinEnergy(kenew)
		Zsync = - math.pi * SyncPhase / (180.0 * ZtoPhi)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode = Harmonic_Cav(ZtoPhi, dESync,\
			RFHNum, RFVoltage, RFPhase)
		self.setType("harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		SyncPhase_tuple = self.localDict["SyncPhase"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		SyncPhase = interp(time, n_tuple,\
			time_tuple, SyncPhase_tuple)
		dESync = charge * RFVoltage *\
			math.sin(math.pi *\
			(RFHNum * SyncPhase + RFPhase) / 180.0)
		kenew = keold + dESync
		bunch.getSyncParticle().kinEnergy(kenew)
		ZtoPhi = self.Z2Phi
		Zsync = - math.pi * SyncPhase / (180.0 * ZtoPhi)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.RFVoltage(RFVoltage)
		self.harmonicnode.RFPhase(RFPhase)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		SyncPhase_tuple = self.localDict["SyncPhase"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		SyncPhase = interp(time, n_tuple,\
			time_tuple, SyncPhase_tuple)
		dESync = charge * RFVoltage *\
			math.sin(math.pi *\
			(RFHNum * SyncPhase + RFPhase) / 180.0)
		kenew = keold + dESync
		bunch.getSyncParticle().kinEnergy(kenew)
		ZtoPhi = self.Z2Phi
		Zsync = - math.pi * SyncPhase / (180.0 * ZtoPhi)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.RFVoltage(RFVoltage)
		self.harmonicnode.RFPhase(RFPhase)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class BRhoDep_Multi_Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, accelDict, bunch,\
		length, name = "brho_timedep_harmonic_rfnode"):
		"""
			Constructor. Creates BRho Time Dependent
			Harmonic RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.Z2Phi = ZtoPhi
		self.localDict = accelDict
		self.gammaTrans = self.localDict["gammaTrans"]
		self.RFHarms = self.localDict["RFHNum"]
                self.nRFHarmonics = self.RFHarms.pop(0)
		self.n_tuple = self.localDict["n_tuple"]
		self.time_tuple = self.localDict["time"]
		self.BRho_tuple = self.localDict["BRho"]
		self.RFVoltage_list = self.localDict["RFVoltage"]
		self.RFPhase_list = self.localDict["RFPhase"]

		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		self.charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		keold = bunch.getSyncParticle().kinEnergy()
		eold = keold + mass
                self.RFVoltages = []
                self.RFPhases = []
                for i in xrange(self.nRFHarmonics):
                    self.RFVoltages.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFVoltage_list[i]))
                    self.RFPhases.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFPhase_list[i]))
                    self.RFHarms[i] = float(self.RFHarms[i])#make sure the numbers are float
                BRho = interp(time, self.n_tuple,\
                    self.time_tuple, self.BRho_tuple)
		pcnew = 0.299792458 * self.charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		self.dESync = enew - eold
                Zsync = self.getZsync(gamma)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode = Multi_Harmonic_Cav(ZtoPhi, self.dESync,\
			self.RFHarms, self.RFVoltages, self.RFPhases, self.nRFHarmonics)
		self.setType("harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#gammaTrans = self.localDict["gammaTrans"]
		#RFHNum = self.localDict["RFHNum"]
		#n_tuple = self.localDict["n_tuple"]
		#time_tuple = self.localDict["time"]
		#BRho_tuple = self.localDict["BRho"]
		#RFVoltage_tuple = self.localDict["RFVoltage"]
		#RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		self.charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		eold = keold + mass
                self.RFVoltages = []
                self.RFPhases = []
                for i in xrange(self.nRFHarmonics):
                    self.RFVoltages.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFVoltage_list[i]))
                    self.RFPhases.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFPhase_list[i]))
                BRho = interp(time, self.n_tuple,\
                    self.time_tuple, self.BRho_tuple)
		pcnew = 0.299792458 * self.charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		self.dESync = enew - eold
                Zsync = self.getZsync(gamma)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(self.dESync)
		self.harmonicnode.RFVoltages(self.RFVoltages)
		self.harmonicnode.RFPhases(self.RFPhases)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#gammaTrans = self.localDict["gammaTrans"]
		#RFHNum = self.localDict["RFHNum"]
		#n_tuple = self.localDict["n_tuple"]
		#time_tuple = self.localDict["time"]
		#BRho_tuple = self.localDict["BRho"]
		#RFVoltage_tuple = self.localDict["RFVoltage"]
		#RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		self.charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		eold = keold + mass
                self.RFVoltages = []
                self.RFPhases = []
                for i in xrange(self.nRFHarmonics):
                    self.RFVoltages.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFVoltage_list[i]))
                    self.RFPhases.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFPhase_list[i]))
                BRho = interp(time, self.n_tuple,\
                    self.time_tuple, self.BRho_tuple)
		pcnew = 0.299792458 * self.charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		self.dESync = enew - eold
                Zsync = self.getZsync(gamma)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(self.dESync)
		self.harmonicnode.RFVoltages(self.RFVoltages)
		self.harmonicnode.RFPhases(self.RFPhases)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

        def dEResid(self, sp):
            '''work around egcs 1.02 problem'''
            dESum = 0.0
            for i in xrange(self.nRFHarmonics):
                dESum += self.charge*self.RFVoltages[i]*math.sin(self.RFHarms[i]*sp+self.RFPhases[i])
            return self.dESync-dESum

        def findGuessRange(self):
            minGuess = -math.pi/(2*self.RFHarms[0])
            maxGuess = math.pi/(2*self.RFHarms[0])
            nSteps = 2000
            step = (maxGuess - minGuess)/nSteps
            x_start = minGuess
            x_up_limit = maxGuess
            for i in xrange(nSteps):
                x0 = x_start + i*step
                x1 = x0 + step
                f0 = self.dEResid(x0)
                f1 = self.dEResid(x1)
                if f0*f1 <= 0 and abs(x1) < x_up_limit:
                    minGuess = x0
                    maxGuess = x1
                    x_up_limit = abs(x1)
            return minGuess, maxGuess

        def getZsync(self, gamma):
            minGuess, maxGuess = self.findGuessRange()
            phase = [0.0]
            tiny = 1e-20
            if abs(self.dEResid(phase[0])) < tiny and (phase[0] < maxGuess and
                    phase[0] > minGuess):
                pass
            else:
                if self.rtbi(phase, self.dEResid, minGuess, maxGuess, 1e-8) == 0:
                    raise ValueError("Bad prescribed voltage / B-field combo in Acceleration!")
            phase = phase[0]
            if gamma > self.gammaTrans and self.gammaTrans > 0:
                phase = math.pi/self.RFHarms[0]- phase
            ZtoPhi = self.Z2Phi
            Zsync = - (phase-math.pi*self.RFPhases[0]/180/self.RFHarms[0]) / ZtoPhi
            return Zsync


class SyncPhaseDep_Multi_Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, accelDict, bunch,\
		length, name = "syncphase_timedep_harmonic_rfnode"):
		"""
			Constructor. Creates SyncPhase Time Dependent
			Harmonic RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.Z2Phi = ZtoPhi
		self.localDict = accelDict
		self.gammaTrans = self.localDict["gammaTrans"]
		self.RFHarms = self.localDict["RFHNum"]
                self.nRFHarmonics = self.RFHarms.pop(0)
		self.n_tuple = self.localDict["n_tuple"]
		self.time_tuple = self.localDict["time"]
		self.SyncPhase_tuple = self.localDict["SyncPhase"]
		self.RFVoltage_list = self.localDict["RFVoltage"]
		self.RFPhase_list = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		SyncPhase = interp(time, n_tuple,\
			time_tuple, SyncPhase_tuple)
                RFVoltages = []
                RFPhases = []
                dESync = 0
                for i in xrange(self.nRFHarmonics):
                    RFVoltages.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFVoltage_list[i]))
                    RFPhases.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFPhase_list[i]))
                    dESync += charge * RFVoltages[i] *\
                        math.sin(math.pi *\
                        (self.RFHarms[i] * SyncPhase + RFPhases[i]) / 180.0)
		kenew = keold + dESync
		bunch.getSyncParticle().kinEnergy(kenew)
		Zsync = - math.pi * SyncPhase / (180.0 * ZtoPhi)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode = Multi_Harmonic_Cav(ZtoPhi, dESync,\
			self.RFHarms, RFVoltages, RFPhases, self.nRFHarmonics)
		self.setType("harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#gammaTrans = self.localDict["gammaTrans"]
		#RFHNum = self.localDict["RFHNum"]
		#n_tuple = self.localDict["n_tuple"]
		#time_tuple = self.localDict["time"]
		#SyncPhase_tuple = self.localDict["SyncPhase"]
		#RFVoltage_tuple = self.localDict["RFVoltage"]
		#RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		SyncPhase = interp(time, n_tuple,\
			time_tuple, SyncPhase_tuple)
                RFVoltages = []
                RFPhases = []
                dESync = 0
                for i in xrange(self.nRFHarmonics):
                    RFVoltages.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFVoltage_list[i]))
                    RFPhases.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFPhase_list[i]))
                    dESync += charge * RFVoltages[i] *\
                        math.sin(math.pi *\
                        (self.RFHarms[i] * SyncPhase + RFPhases[i]) / 180.0)
		kenew = keold + dESync
		bunch.getSyncParticle().kinEnergy(kenew)
		ZtoPhi = self.Z2Phi
		Zsync = - math.pi * SyncPhase / (180.0 * ZtoPhi)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.RFVoltages(RFVoltages)
		self.harmonicnode.RFPhases(RFPhases)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#gammaTrans = self.localDict["gammaTrans"]
		#RFHNum = self.localDict["RFHNum"]
		#n_tuple = self.localDict["n_tuple"]
		#time_tuple = self.localDict["time"]
		#SyncPhase_tuple = self.localDict["SyncPhase"]
		#RFVoltage_tuple = self.localDict["RFVoltage"]
		#RFPhase_tuple = self.localDict["RFPhase"]
		time = bunch.getSyncParticle().time()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		SyncPhase = interp(time, n_tuple,\
			time_tuple, SyncPhase_tuple)
                RFVoltages = []
                RFPhases = []
                dESync = 0
                for i in xrange(self.nRFHarmonics):
                    RFVoltages.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFVoltage_list[i]))
                    RFPhases.append(interp(time, self.n_tuple,\
                        self.time_tuple, self.RFPhase_list[i]))
                    dESync += charge * RFVoltages[i] *\
                        math.sin(math.pi *\
                        (self.RFHarms[i] * SyncPhase + RFPhases[i]) / 180.0)
		kenew = keold + dESync
		bunch.getSyncParticle().kinEnergy(kenew)
		ZtoPhi = self.Z2Phi
		Zsync = - math.pi * SyncPhase / (180.0 * ZtoPhi)
		bunch.getSyncParticle().z(Zsync)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.RFVoltages(RFVoltages)
		self.harmonicnode.RFPhases(RFPhases)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class Barrier_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, RFVoltage, RFPhasep,\
		RFPhasem, dRFPhasep, dRFPhasem,\
		length, name = "barrier_rfnode"):
		"""
			Constructor. Creates Barrier
			RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.barriernode = Barrier_Cav(ZtoPhi, RFVoltage,\
			RFPhasep, RFPhasem, dRFPhasep, dRFPhasem)
		self.setType("barrier rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		#put the track method here:
		self.barriernode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		#put the track method here:
		self.barriernode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

class TimeDep_Barrier_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, accelDict, bunch,\
		length, name = "syncphase_timedep_harmonic_rfnode"):
		"""
			Constructor. Creates SyncPhase Time Dependent
			Harmonic RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.localDict = accelDict
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhasep_tuple  = self.localDict["RFPhasep"]
		RFPhasem_tuple  = self.localDict["RFPhasem"]
		dRFPhasep_tuple = self.localDict["dRFPhasep"]
		dRFPhasem_tuple = self.localDict["dRFPhasem"]
		time = bunch.getSyncParticle().time()
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhasep = interp(time, n_tuple,\
			time_tuple, RFPhasep_tuple)
		RFPhasem = interp(time, n_tuple,\
			time_tuple, RFPhasem_tuple)
		dRFPhasep = interp(time, n_tuple,\
			time_tuple, dRFPhasep_tuple)
		dRFPhasem = interp(time, n_tuple,\
			time_tuple, dRFPhasem_tuple)
		self.barriernode = Barrier_Cav(ZtoPhi, RFVoltage,\
			RFPhasep, RFPhasem, dRFPhasep, dRFPhasem)
		self.setType("barrier rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhasep_tuple  = self.localDict["RFPhasep"]
		RFPhasem_tuple  = self.localDict["RFPhasem"]
		dRFPhasep_tuple = self.localDict["dRFPhasep"]
		dRFPhasem_tuple = self.localDict["dRFPhasem"]
		time = bunch.getSyncParticle().time()
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhasep = interp(time, n_tuple,\
			time_tuple, RFPhasep_tuple)
		RFPhasem = interp(time, n_tuple,\
			time_tuple, RFPhasem_tuple)
		dRFPhasep = interp(time, n_tuple,\
			time_tuple, dRFPhasep_tuple)
		dRFPhasem = interp(time, n_tuple,\
			time_tuple, dRFPhasem_tuple)
		self.barriernode.RFVoltage(RFVoltage)
		self.barriernode.RFPhasep(RFPhasep)
		self.barriernode.RFPhasem(RFPhasem)
		self.barriernode.dRFPhasep(dRFPhasep)
		self.barriernode.dRFPhasem(dRFPhasem)
		#put the track method here:
		self.barriernode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhasep_tuple  = self.localDict["RFPhasep"]
		RFPhasem_tuple  = self.localDict["RFPhasem"]
		dRFPhasep_tuple = self.localDict["dRFPhasep"]
		dRFPhasem_tuple = self.localDict["dRFPhasem"]
		time = bunch.getSyncParticle().time()
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhasep = interp(time, n_tuple,\
			time_tuple, RFPhasep_tuple)
		RFPhasem = interp(time, n_tuple,\
			time_tuple, RFPhasem_tuple)
		dRFPhasep = interp(time, n_tuple,\
			time_tuple, dRFPhasep_tuple)
		dRFPhasem = interp(time, n_tuple,\
			time_tuple, dRFPhasem_tuple)
		self.barriernode.RFVoltage(RFVoltage)
		self.barriernode.RFPhasep(RFPhasep)
		self.barriernode.RFPhasem(RFPhasem)
		self.barriernode.dRFPhasep(dRFPhasep)
		self.barriernode.dRFPhasem(dRFPhasem)
		#put the track method here:
		self.barriernode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length
		#print "time = ", time
		#print "RFVoltage = ", RFVoltage
		#print "RFPhaseplus = ", RFPhasep + dRFPhasep, RFPhasep - dRFPhasep
		#print "RFPhasemnus = ", RFPhasem - dRFPhasem, RFPhasem + dRFPhasem

def interp(x, n_tuple, x_tuple, y_tuple):
	"""
	Linear interpolation: Given n-tuple + 1 points,
	x_tuple and y_tuple, routine finds y = y_tuple
	at x in x_tuple. Assumes x_tuple is increasing array.
	"""
	if x <= x_tuple[0]:
		y = y_tuple[0]
		return y
	if x >= x_tuple[n_tuple]:
		y = y_tuple[n_tuple]
		return y
	dxp = x - x_tuple[0]
	for n in range(n_tuple):
		dxm = dxp
		dxp = x - x_tuple[n + 1]
		dxmp = dxm * dxp
		if dxmp <= 0:
			break
	y = (-dxp * y_tuple[n] + dxm * y_tuple[n + 1]) /\
		(dxm - dxp)
	return y

def syncZ(ZtoPhi, gammaTrans, gamma, charge,\
	dESync, RFHNum, RFVoltage, RFPhase):
	"""
	Calculates position of synchronous particle.
	"""
	Zsync = 0
	if abs(dESync) > abs(charge * RFVoltage):
		return Zsync
	PhaseTot = math.asin(dESync / (charge * RFVoltage))
	if gamma > gammaTrans and gammaTrans > 0:
		PhaseTot = math.pi - PhaseTot
	Zsync = -(PhaseTot - math.pi * RFPhase / 180.0)\
		/ (RFHNum * ZtoPhi)
	return Zsync

#.........dual RFNode for csns , writen by liuhy@ihep.ac.cn .............
class BRhoDep_Dual_Harmonic_RFNode(Base_RFNode):

	def __init__(self, ZtoPhi, accelDict, bunch,\
		length, name = "brho_timedep_dual_harmonic_rfnode"):
		"""
			Constructor. Creates BRho Time Dependent
			Harmonic RF Cavity TEAPOT element
		"""
		Base_RFNode.__init__(self, length, name)
		self.Z2Phi = ZtoPhi
		self.localDict = accelDict
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		BRho_tuple = self.localDict["BRho"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		RatioRFHNum = self.localDict["RatioRFHNum"]
		RatioVoltage_tuple = self.localDict["RatioVoltage"]
		RFPhase2_tuple = self.localDict["RFPhase2"]
		if "longTrackOnly" in self.localDict.keys():
		    longTrackOnly = int(self.localDict["longTrackOnly"])
		else:
			longTrackOnly = 0
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		keold = bunch.getSyncParticle().kinEnergy()
		pcold = bunch.getSyncParticle().momentum()
		eold = keold + mass
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase1 = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		BRho = interp(time, n_tuple,\
			time_tuple, BRho_tuple)
		RatioVoltage = interp(time, n_tuple,\
			time_tuple, RatioVoltage_tuple)
		RFPhase2 = interp(time, n_tuple,\
			time_tuple, RFPhase2_tuple)
		pcnew = 0.299792458 * charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		dESync = enew - eold
		Zsync = syncZ_dual(ZtoPhi, gammaTrans, gamma, charge,\
			dESync, RFHNum, RFVoltage,RatioVoltage, RFPhase1, RFPhase2)
		bunch.getSyncParticle().z(Zsync)
		SyncPhase = -RFHNum*Zsync*ZtoPhi/2/math.pi*360 #RFPhase 1 means Synctron Phase when only the foundmental harmonic exsits
		self.harmonicnode = Dual_Harmonic_Cav(ZtoPhi, dESync, RFHNum, RatioRFHNum, RFVoltage, RatioVoltage, SyncPhase, RFPhase1, RFPhase2, gammaTrans, longTrackOnly)
		self.setType("dual harmonic rf node")
		self.setLength(0.0)

	def trackBunch(self, bunch):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		BRho_tuple = self.localDict["BRho"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		RatioRFHNum = self.localDict["RatioRFHNum"]
		RatioVoltage_tuple = self.localDict["RatioVoltage"]
		RFPhase2_tuple = self.localDict["RFPhase2"]
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		pcold = bunch.getSyncParticle().momentum()
		eold = keold + mass
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase1 = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		BRho = interp(time, n_tuple,\
			time_tuple, BRho_tuple)
		RatioVoltage = interp(time, n_tuple,\
			time_tuple, RatioVoltage_tuple)
		RFPhase2 = interp(time, n_tuple,\
			time_tuple, RFPhase2_tuple)
		pcnew = 0.299792458 * charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		dESync = enew - eold
		ZtoPhi = self.Z2Phi
		Zsync = syncZ_dual(ZtoPhi, gammaTrans, gamma, charge,\
			dESync, RFHNum, RFVoltage,RatioVoltage, RFPhase1,RFPhase2)
		bunch.getSyncParticle().z(Zsync)
		SyncPhase = -RFHNum*Zsync*ZtoPhi/2/math.pi*360
		self.harmonicnode.RFHNum(RFHNum)
		self.harmonicnode.RFVoltage(RFVoltage)
		self.harmonicnode.RFPhase(SyncPhase)
		self.harmonicnode.RatioRFHNum(RatioRFHNum)
		self.harmonicnode.RatioVoltage(RatioVoltage)
		self.harmonicnode.RFPhase1(RFPhase1)
		self.harmonicnode.RFPhase2(RFPhase2)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

	def track(self, paramsDict):
		"""
			The rfcavity-teapot class implementation of the
			AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		gammaTrans = self.localDict["gammaTrans"]
		RFHNum = self.localDict["RFHNum"]
		n_tuple = self.localDict["n_tuple"]
		time_tuple = self.localDict["time"]
		BRho_tuple = self.localDict["BRho"]
		RFVoltage_tuple = self.localDict["RFVoltage"]
		RFPhase_tuple = self.localDict["RFPhase"]
		RatioRFHNum = self.localDict["RatioRFHNum"]
		RatioVoltage_tuple = self.localDict["RatioVoltage"]
		RFPhase2_tuple = self.localDict["RFPhase2"]
		time = bunch.getSyncParticle().time()
		mass = bunch.mass()
		charge = bunch.charge()
		gamma = bunch.getSyncParticle().gamma()
		beta = bunch.getSyncParticle().beta()
		keold = bunch.getSyncParticle().kinEnergy()
		pcold = bunch.getSyncParticle().momentum()
		eold = keold + mass
		RFVoltage = interp(time, n_tuple,\
			time_tuple, RFVoltage_tuple)
		RFPhase1 = interp(time, n_tuple,\
			time_tuple, RFPhase_tuple)
		BRho = interp(time, n_tuple,\
			time_tuple, BRho_tuple)
		RatioVoltage = interp(time, n_tuple,\
			time_tuple, RatioVoltage_tuple)
		RFPhase2 = interp(time, n_tuple,\
			time_tuple, RFPhase2_tuple)
		pcnew = 0.299792458 * charge * BRho
		enew = math.sqrt(pcnew * pcnew + mass * mass)
		kenew = enew - mass
		bunch.getSyncParticle().kinEnergy(kenew)
		dESync = enew - eold
		ZtoPhi = self.Z2Phi
		Zsync = syncZ_dual(ZtoPhi, gammaTrans, gamma, charge,\
			dESync, RFHNum, RFVoltage,RatioVoltage, RFPhase1, RFPhase2)
		bunch.getSyncParticle().z(Zsync)
		SyncPhase = -RFHNum*Zsync*ZtoPhi/2/math.pi*360
		self.harmonicnode.RFHNum(RFHNum)
		self.harmonicnode.RFVoltage(RFVoltage)
		self.harmonicnode.RFPhase(SyncPhase)
		self.harmonicnode.RatioRFHNum(RatioRFHNum)
		self.harmonicnode.RatioVoltage(RatioVoltage)
		self.harmonicnode.RFPhase1(RFPhase1)
		self.harmonicnode.RFPhase2(RFPhase2)
		self.harmonicnode.dESync(dESync)
		self.harmonicnode.OldGamma(gamma)
		self.harmonicnode.OldBeta(beta)
		#put the track method here:
		self.harmonicnode.trackBunch(bunch)
		#print "debug tracking the bunch through the rf node = ",\
		self.getName(), " part ind = ", self.getActivePartIndex(),\
		" length = ", length

def syncZ_dual(ZtoPhi, gammaTrans, gamma, charge,\
	dESync, RFHNum, RFVoltage,RatioVoltage, RFPhase1,RFPhase2):
	"""
	Calculates position of synchronous particle.
	"""
	Zsync = 0
	if abs(dESync) > abs(charge * RFVoltage):
		return Zsync
	PhaseTot = math.asin(dESync / (charge * RFVoltage)+RatioVoltage*math.sin( RFPhase2*math.pi / 180.0 ))
        if gamma > gammaTrans and gammaTrans > 0:
		PhaseTot = math.pi - PhaseTot
	Zsync = -(PhaseTot - math.pi * RFPhase1 / 180.0)\
		/ (RFHNum * ZtoPhi)
	return Zsync

